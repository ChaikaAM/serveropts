# README #

This repository contains branches with option files from server to be compared for development purposes

### What is this repository for? ###

This server is used by ScanOpts app installed at develop servers.
ScanOpts app pushes option files there and their changes in real time.
Every server options are stored in special branch like "servername" branch

### How do I get set up? ###

No need to any set up. Just compare branches with options from different servers

test commit